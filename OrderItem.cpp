#include "OrderItem.h"
#include "Item.h"
#include "Order.h"
#include <iostream>

OrderItem::OrderItem() {
	this->qtt = 0;
	this->value = 0;
}

OrderItem::~OrderItem() {
}

OrderItem OrderItem::operator ++ () {
	if(this->item.getQtt() > 0) {
		this->item.setQtt(this->item.getQtt() - 1);
		this->qtt += 1;
		this->value += this->item.getPrice();
	}
	return *this;

}
OrderItem OrderItem::operator -- () {
	
	if(this->qtt > 0) {
		this->qtt -= 1;
		this->item.setQtt(this->item.getQtt() + 1);
		this->value -= this->item.getPrice();
	}
	return *this;

}

int OrderItem::getQtt() {
	return this->qtt;
}

float OrderItem::getValue() {
	return this->value;
}

Item OrderItem::getItem() {
	return this->item;
}

void OrderItem::setItem(Item* item) {
	//std::cout << item->getQtt() << "qtt" << std::endl;
	//std::cout << item->getPrice() << "Price" << std::endl;
	//std::cout << item->getName() << "name" << std::endl;
	
	this->item.setName(item->getName());
	this->item.setPrice(item->getPrice());
	this->item.setQtt(item->getQtt());
}
