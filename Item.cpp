#include "Item.h"
#include <vector>
#include <iostream>
#include <sstream>
#include <string>

Item::Item() {
	this->qtt = 0;
	this->name = "";
	this->price = 0;
}

Item::~Item() {
}

std::vector<Item> Item::itens;


std::vector<Item>* Item::getAll() {
	return &Item::itens;
}

void Item::addItem() {
	if(this->name != "" && this->price > 0) itens.push_back(*this);
	else std::cout << "N�o consegue, n� Moises!" << std::endl;
}

void Item::seed() {
	Item aux;
	std::ostringstream ss;
	ss << (Item::itens.size() + 1);
	
	aux.setName("Biscoito " + ss.str());
	aux.setPrice(10.5);
	aux.setQtt(10);
	aux.addItem();
}

void Item::seed(int i) {
	if (i <= 0) return;
	for(int j = 0; j < i; ++j) {
		seed();
	}
	
}

std::string Item::toStr() {
	std::string aux = "";
    std::ostringstream qtt, price;
    
    qtt << this->qtt;
    price << this->price;
    
	aux = "Quantidade: " + qtt.str() + "; Nome: " + this->name + "; Preco: R$ " + price.str() + ";" ;
	return aux;
}

int Item::getQtt() {
	return this->qtt;
}

void Item::setQtt(int qtt) {
	this->qtt = qtt;
}

std::string Item::getName() {
	return this->name;
}

void Item::setName(std::string name) {
	this->name = name;
}

float Item::getPrice() {
	return this->price;
}

void Item::setPrice(float price) {
	this->price = price;
}

bool Item::isNull() {
	if(this->name == "" && this->price == 0 && this->qtt == 0) return true;
	else return false;
}
