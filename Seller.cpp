#include "Seller.h"
#include "Item.h"
#include "Order.h"
#include <vector>
#include <string>
#include <iostream>
#include <sstream>

std::vector<Seller> Seller::sellers;
std::vector<Order> Seller::orders;

Seller::Seller() {
    this->Order_Index = 0;
}

Seller::~Seller() {
}

std::vector<Seller> *Seller::getSellers() {
    return &this->sellers;
}

void Seller::sellerseed(int i) {
    if (i <= 0) return;
    for (int j = 0; j < i; ++j) {
        Seller::sellerseed();
    }
}


void Seller::sellerseed() {

    Seller aux;
    std::ostringstream ss;
    ss << (Seller::sellers.size() + 1);

    aux.setName("Vendedor " + ss.str());
    aux.setEmail("vendedor" + ss.str() + "@gmail.com");
    aux.setGrr("GRR2015" + ss.str());
    aux.setPassword("senha" + ss.str() + "xx");

    Seller::sellers.push_back(aux);
}

void Seller::printSellers() {
    std::cout << "Nesse momento temos " << sellers.size() << " vendedores." << std::endl;
}

void Seller::sell(int Order_Index) {
    return this->orders.at(Order_Index).setStatus(SELLED);
}


User *Seller::login() {
    for (int i = 0; i < sellers.size(); ++i) {
        if (this->getEmail() == (sellers.at(i)).getEmail() &&
            this->getPassword() == (sellers.at(i)).getPassword())
            return &sellers.at(i);
    }
    return NULL;
}