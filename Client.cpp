#include "Client.h"
#include "User.h"
#include <vector>
#include <iostream>
#include <sstream>

std::vector<Client> Client::clients;

std::vector<Client>* Client::getClients() {
	return &Client::clients;
}

void Client::clientSeed(int i) {
	if (i <= 0) return;
	for(int j = 0; j < i; ++j) {
		Client::clientSeed();
	}
}

void Client::clientSeed() {
	
	Client aux;
	std::ostringstream ss;
	ss << (Client::clients.size() + 1);
	
	aux.setBalance(100 * Client::clients.size());
	aux.setName("Cliente Teste " + ss.str());
	aux.setEmail("cliente" + ss.str() + "@gmail.com");
	aux.setGrr("GRR2017" + ss.str());
	aux.setPassword("senha" + ss.str() + "xx");
	
	Client::clients.push_back(aux);
}


Client::Client() : User::User() {
	this->balance = 0;
	this->orders.clear();
	this->currentOrder.clearAtualOrder();
}

Client::~Client() {
}

void Client::addItem(Item* item) {
	this->currentOrder + *item;
}

User* Client::login() {
	for(int i = 0; i < clients.size(); ++i) {
		if(this->getEmail() == (clients.at(i)).getEmail() && this->getPassword() == (clients.at(i)).getPassword()) return &clients.at(i);
	}
	return NULL;
}

float Client::getBalance() {
	return this->balance;
}

void Client::setBalance(float balance) {
	this->balance = balance;
}

std::vector<Order>* Client::getOrders() {
	return &this->orders;
}

Order* Client::getCurrentOrder() {
	return &this->currentOrder;
}

