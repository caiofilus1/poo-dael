#include "Order.h"
#include "OrderItem.h"
#include <vector>
#include <iostream>

Order::Order() {
	this->price = 0;
	this->status = CREATING;
}

Order::~Order() {
}

std::vector<Order> Order::orders;

std::vector<Order>* Order::getAll() {
	return &Order::orders;
}

float Order::getPrice() {
	return this->price;
}

void Order::operator - (Item aux) {
	//this->std::vector<OrderItem>;
	if(aux.isNull()) return;
	
	//int n = 0;
	bool x = false;
	
	for(int i = 0; i < this->orderItens.size(); ++i) {
		if((this->orderItens.at(i)).getItem().getName() == aux.getName()) {
			//std::cout << "Achou!";
			
			//std::cout << (this->orderItens.at(i)).getQtt() << "qtdade : Dentro do --; Antes" << std::endl;
			--(this->orderItens.at(i));
			//std::cout << (this->orderItens.at(i)).getQtt() << "qtdade : Dentro do --; Depois" << std::endl;
			
			if((this->orderItens.at(i)).getQtt() <= 0) {
				this->orderItens.erase(this->orderItens.begin()+i);
				//std::cout << "Deletado!!!" << std::endl;
			}
			
			
			//n=i;
			x=true;
			continue;
		}
	}
	if(!x) std::cout << "Nunca nem vi!" << std::endl;
	
	this->setPrice();
}

void Order::operator + (Item aux) {
	//this->std::vector<OrderItem>;
	if(aux.isNull()) return;
	
	bool x = false;
	int n = 0;
	
	for(int i = 0; i < this->orderItens.size(); ++i) {
		if((this->orderItens.at(i)).getItem().getName() == aux.getName()) {
			//std::cout << "Achou!";
			n=i;
			x=true;
			continue;
		}
	}
	if(x) {
		++(this->orderItens.at(n));
	}
	else {
		int tamFUTURO = this->orderItens.size();
		OrderItem oi;
		oi.setItem(&aux);
		//std::cout << oi.getItem().getName() << std::endl;
		this->orderItens.push_back(oi);

		//this->orderItens.push_back()
		
		++(this->orderItens.at(tamFUTURO));
	}
	
	this->setPrice();
}

Status Order::getStatus() {
	return this->status;
}

void Order::setStatus(Status status) {
	this->status = status;
}

void Order::save() {
	if(this->price == 0) std::cout << "Opa fi�o!" << std::endl;
	else orders.push_back(*this);
}

std::vector<OrderItem>* Order::OI() {
	return &this->orderItens;
}

void Order::setPrice() {
	float aux = 0;
	for(int i = 0; i < this->orderItens.size(); ++i) {
		aux += (this->orderItens.at(i)).getValue();
	}
	this->price = aux;
}

void Order::clearAtualOrder() {
	this->orderItens.clear();
}
