#ifndef USER_H
#define USER_H

#include <vector>
#include <iostream>

class User {
public:
    User();

    ~User();

    virtual User *login();

    void create();

    std::string getName();

    void setName(std::string);

    std::string getEmail();

    void setEmail(std::string);

    std::string getPassword();

    void setPassword(std::string);

    std::string getGrr();

    void setGrr(std::string);

    std::vector<User> *getUsers();

    void printNumUsers();


protected:

private:
    std::string name;
    std::string email;
    std::string password;
    std::string grr;
    static std::vector<User> users;

};

#endif
