#ifndef ITEM_H
#define ITEM_H

#include <vector>
#include <iostream>

class Item {
	public:
		Item();
		~Item();
		
		static std::vector<Item>* getAll();
		static void seed();
		static void seed(int);
		
		void addItem();
		
		std::string toStr();
		
		int getQtt();
		void setQtt(int);
		std::string getName();
		void setName(std::string);
		float getPrice();
		void setPrice(float);
		
		bool isNull();
		
		
	protected:
		
	private:
		int qtt;
		std::string name;
		float price;
		static std::vector<Item> itens;
};

#endif
