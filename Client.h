#ifndef CLIENTE_H
#define CLIENTE_H

#include "User.h"
#include "Order.h"
#include "Item.h"
#include "User.h"
#include <vector>
#include <iostream>

class Client : public User {
	public:
		Client();
		~Client();
		
		static std::vector<Client>* getClients();
		
		void addItem(Item*);
		User* login();
		
		float getBalance();
		void setBalance(float);
		std::vector<Order>* getOrders();
		Order* getCurrentOrder();
		
		static void clientSeed();
		static void clientSeed(int);
		
		
	protected:
		
	private:
		float balance;
		std::vector<Order> orders;
		Order currentOrder;
		static std::vector<Client> clients;

};

#endif
