#ifndef SELLER_H
#define SELLER_H

#include "User.h"
#include "Order.h"
#include "Item.h"
#include <vector>
#include <iostream>

class Seller : public User {
public:
    Seller();

    ~Seller();

    static void sellerseed();

    static void sellerseed(int);

    User *login();

    std::vector<Seller> *getSellers();

    void printSellers();


    void sell(int);

//		Item* addItem(Item*);



protected:

private:

    int Order_Index;

    static std::vector<Seller> sellers;
    static std::vector<Order> orders;

};

#endif
