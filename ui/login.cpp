//
// Created by caiof on 13/09/2020.
//

#include "login.h"
#include <iostream>
#include <string>
#include "../Client.h"
#include "../User.h"
#include "../Seller.h"
#include "ClientMenu.h"
#include "AdiminMenu.h"

using namespace std;

void login() {

    bool exit = false;
    while (!exit) {
        cout << "Para fazer login Digite seu Email e Senha" << endl;
        string email;
        string password;
        cout << "Email: ";
        cin >> email;
        cout << "Senha: ";
        cin >> password;
        User *loggedUser = nullptr;
        Client client;
        client.setEmail(email);
        client.setPassword(password);
        loggedUser = client.login();
        if (loggedUser == nullptr) {
            Seller seller;
            seller.setEmail(email);
            seller.setPassword(password);
            loggedUser = seller.login();
        }
        if (loggedUser == nullptr) {
            cerr << "Email ou senha Errados, tente novamente" << endl;
        } else {
            cout << "Ola " << loggedUser->getName() << " GRR: " << loggedUser->getGrr() << endl;
            if (dynamic_cast<Client *>(loggedUser)) {
                clientMenu(dynamic_cast<Client *>(loggedUser));
            } else {
                adiminMenu(dynamic_cast<Seller *>(loggedUser));
                //TODO go to ADMIN menu
            }
        }
    }

}