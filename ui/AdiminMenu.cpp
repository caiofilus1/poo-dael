# include "../Seller.h"
#include "AdiminMenu.h"
#include "ItemList.h"
#include "SellPage.h"
#include <iostream>
#include <string>

//To usando o item lista como modelo pra ficar parecido uma pagina com a outra
using namespace std;

typedef enum MENU_OPTION {
    DEFAULT = 0, RETURN, SELL_ITENS, PRODUTOS
} MenuOption;

istream &operator>>(istream &is, MENU_OPTION &i) {
    int tmp;
    if (is >> tmp)
        i = static_cast<MENU_OPTION>( tmp );
    return is;
}


void adiminMenu(Seller *seller) {
    cout << "-----------------------" << endl;
    cout
            << "Selecione o valor correspondente para o que deseja " << endl
            << "1 para retornar" << endl
            << "2 para vender itens" << endl
            << "3 para Acessar a pagina de produtos" << endl;
    cout << "-----------------------" << endl;
    MenuOption menuOption = DEFAULT;
    cin >> menuOption;
    switch (menuOption) {
        case RETURN: {
            return;
        }
        case SELL_ITENS: {
            sellPage(seller);
        }
        case PRODUTOS: {
            itemList();
        }
    }
    adiminMenu(seller);

}


