//
// Created by caiof on 14/09/2020.
//

#include "SellPage.h"
#include <vector>
#include <iostream>
#include <string>

using namespace std;

string statusToStr(Order *order) {
    switch (order->getStatus()) {
        case CREATING:
            return "Criando";
        case PENDENT:
            return "Aberto";
        case SELLED:
            return "Vendido";
    };
}


void sellPage(Seller *seller) {
    vector<Order> *orders = Order::getAll();
    cout << "Total de Itens: " << orders->size() << endl;
    int i = 1;
    for (auto &item : *orders) {
        cout << "----------------" << endl;
        cout << "seletor: " << i << endl;
        cout << "total: " << item.getPrice() << endl;
        cout << "status: " << statusToStr(&item) << endl;
        cout << "itens: " << endl;
        for (auto &orderItem : *item.OI()) {
            cout << "   " << orderItem.getQtt() << "x" << orderItem.getItem().getName() << " = "
                 << orderItem.getValue() << endl;
        }
    }
    cout << "++++++++++++++++++" << endl;
    cout << "Deseja ?" << endl;
    cout << "0 - Retornar" << endl;
    cout << "1~" << orders << " - Vender" << endl;
    int itemOption = -1;
    cin >> itemOption;
    if (itemOption == 0) {
        return;
    }
    seller->sell(itemOption - 1);
    return sellPage(seller);
}