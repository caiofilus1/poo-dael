//
// Created by caiof on 14/09/2020.
//

#include "ClientMenu.h"
#include <iostream>
#include <string>
#include <conio.h>
#include <stdio.h>

using namespace std;

typedef enum USER_OPTION {
    DEFAULT = 0, RETURN, PRODUCT_LIST, ORDER_LIST
} UserOption;

istream &operator>>(istream &is, UserOption &i) {
    int tmp;
    if (is >> tmp)
        i = static_cast<UserOption>( tmp );
    return is;
}


void clientMenu(Client *client) {
    cout << "-----------------------" << endl;
    cout << "Menu do Usuário" << endl;
    cout << "1 - Retornar" << endl;
    cout << "2 - Lista de Produtos" << endl;
    cout << "3 - Lista de Compras" << endl;

    UserOption menuOption = DEFAULT;
    cin >> menuOption;
    switch (menuOption) {
        case RETURN: {
            return;
        }
        case PRODUCT_LIST: {
            //Call product List
            break;
        }
        case ORDER_LIST: {
            cout << string(50, '\n');
            for (auto &order : *client->getOrders()) {
                cout << "----------------" << endl;
                cout << "total: " << order.getPrice() << endl;
                cout << "status: " << order.getStatus() << endl;
                cout << "itens: " << endl;
                for (auto &orderItem : *order.OI()) {
                    cout << "   " << orderItem.getQtt() << "x" << orderItem.getItem().getName() << " = "
                         << orderItem.getValue() << endl;
                }
            }
            break;
        }
    }
    clientMenu(client);
}