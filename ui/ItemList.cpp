//
// Created by caiof on 13/09/2020.
//

#include "ItemList.h"
#include "../Item.h"
#include <vector>
#include <iostream>
#include <string>
#include <conio.h>
#include <stdio.h>

using namespace std;

typedef enum ITEM_OPTION {
    DEFAULT = 0, RETURN, ADD_ITEM
} ItemOption;

istream &operator>>(istream &is, ITEM_OPTION &i) {
    int tmp;
    if (is >> tmp)
        i = static_cast<ITEM_OPTION>( tmp );
    return is;
}

void itemList() {
    vector<Item> *items = Item::getAll();
    cout << "Total de Itens: " << items->size() << endl;
    for (auto &item : *items) {
        cout << "-----------------------" << endl;
        cout << item.toStr() << endl;
    }
    cout << "++++++++++++++++++" << endl;
    cout << "Deseja ?" << endl;
    cout << "1 - Retornar" << endl;
    cout << "2 - Adicionar Item" << endl;
    ItemOption itemOption = DEFAULT;
    cin >> itemOption;
    switch (itemOption) {
        case RETURN: {
            return;
        }
        case ADD_ITEM: {
            cout << string(50, '\n');
            Item newItem;
            string name;
            int qtt;
            float price;
            cout << "Nome do Item:";
            cin >> name;
            cout << "Qtt do Item:";
            cin >> qtt;
            cout << "Preco do Item:";
            cin >> price;
            newItem.setName(name);
            newItem.setQtt(qtt);
            newItem.setPrice(price);
            newItem.addItem();
            cout << string(50, '\n');
            return itemList();
    }
}

}