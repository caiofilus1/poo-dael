#ifndef ORDER_H
#define ORDER_H

#include "OrderItem.h"
#include <vector>
#include <iostream>

enum Status { CREATING, PENDENT, SELLED };


class Order {
	public:
		Order();
		~Order();
		
		static std::vector<Order>* getAll();
		
		float getPrice();
		Status getStatus();
		void setStatus(Status);
		void save();
		std::vector<OrderItem>* OI();
		void clearAtualOrder();
		
		void operator - (Item);
		void operator + (Item);
		
	protected:
		Status status;
		float price;
		
		std::vector<OrderItem> orderItens;
		
		static std::vector<Order> orders;
		
	private:
		void setPrice();
	
};

#endif
