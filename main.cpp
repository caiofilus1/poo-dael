#include <iostream>
#include <vector>
#include <string>
#include "ui/login.h"
#include "Client.h"
#include "Seller.h"

int main() {
    std::cout << "------Bem Vindo Ao Sistema de Gerenciamento de Vendas do Dael--------" << std::endl;
    Client::clientSeed(1);
    Seller::sellerseed(3);
    login();
    return 0;
}
