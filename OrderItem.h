#ifndef ORDERITEM_H
#define ORDERITEM_H

#include "Item.h"
#include <vector>
#include <iostream>

class OrderItem {
	public:
		OrderItem();
		~OrderItem();
		
		OrderItem operator ++();
		OrderItem operator --();
		
		int getQtt();
		float getValue();
		Item getItem();
		void setItem(Item*);
		
		
	protected:
		Item item;
		int qtt;
		float value;
		
	private:	
};

#endif
